import os
from flask import Flask
from dotenv import load_dotenv
from flask_assets import Bundle, Environment


# load env variables
load_dotenv(verbose=True)
secret_key = os.getenv('SECRET_KEY', default=None)


assets = Environment()

# init css assets
css = Bundle('css/layouts.css', 'css/index.css', output='gen/main.css')


def create_app():
    app = Flask(__name__)
    # app configurations
    app.config['SECRET_KEY'] = secret_key

    # init assets && register assets
    assets.init_app(app)
    assets.register('css_all', css)

    # any other package configuration ...

    # import Blueprint && register blueprint
    from .views import api
    app.register_blueprint(api)

    return app
