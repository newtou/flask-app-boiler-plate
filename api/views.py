# all views should come here
from flask import Blueprint, render_template, url_for, redirect, request

api = Blueprint('api', __name__)


@api.route('/')
def index():
    return render_template('index.html')
