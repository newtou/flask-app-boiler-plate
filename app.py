# this is the main file to run the application
from api import create_app

app = create_app()


if __name__ == '__main__':
    app.run()